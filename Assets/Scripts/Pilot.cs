﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pilot : MonoBehaviour
{
    [Header("General")]
    // control de velocidad
    [Tooltip("in ms^-1")][SerializeField] float speedControl = 10f; 
    // limite de desplazamiento horizontal
    [Tooltip("in m")] [SerializeField] float xRange = 6f; 
    // limite de desplazamiento vertical
    [Tooltip("in m")] [SerializeField] float yRange = 3f; 
    // lista para sistemas de particulas que se usaran como armas   
    [SerializeField] GameObject[] guns; 

    [Header("Screen position")]
    // factor de posicionamiento vertical
    [SerializeField] float positionPitchFactor = -2f; 
    // factor de posicionamiento horizontal
    [SerializeField] float positionYawFactor = 7f; 

    [Header("Throw factors")]
    // control de elevacion
    [SerializeField] float controlPitchFactor = -25f;
    // control de rotacion sobre eje frontal
    [SerializeField] float controlRollFactor = -25f; 

    // proyecciones angulares
    float xThrow, yThrow; 
    // control de jugador = habilitado
    bool isControlEnabled = true; 

    // Update se ejecuta en cada cuadro
    void Update()
    {
        if (isControlEnabled)
        {
            ProcessTranlation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    public void OnPlayerDeath()
    {   // referenciado por nombre funcion como cadena
        isControlEnabled = false;
    }

    void ProcessRotation()
    {
        // procesa la rotacion usando angulos de Euler
        float pitchDueToPosition = transform.localPosition.y * positionPitchFactor;
        float pitchDueToThrow = yThrow * controlPitchFactor;
        float pitch = pitchDueToPosition + pitchDueToThrow;

        float yaw = transform.localPosition.x * positionYawFactor;
        float roll = xThrow * controlRollFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessTranlation()
    {
        // procesa el movimiento de la nave en la pantalla
        // limitandose a los parametros descritos anteriormente
        xThrow = Input.GetAxis("Horizontal");
        float xOffset = xThrow * speedControl * Time.deltaTime;
        float rawXPos = transform.localPosition.x + xOffset;
        float clampedXPos = Mathf.Clamp(rawXPos, -xRange, xRange);

        yThrow = Input.GetAxis("Vertical");
        float yOffset = yThrow * speedControl * Time.deltaTime;
        float rawYPos = transform.localPosition.y + yOffset;
        float clampedYPos = Mathf.Clamp(rawYPos, -yRange, yRange);

        transform.localPosition = new Vector3(clampedXPos, clampedYPos, transform.localPosition.z);
    }

    void ProcessFiring()
    {
        // verifica la condicion de disparo y activa o desactiva el mismo
        if (Input.GetKey(KeyCode.Space))
        {
            SetActiveGuns(true);
        }
        else
        {
            SetActiveGuns(false);
        }
    }

    void SetActiveGuns(bool isActive)
    {
        // asigna el parametro recibido al valor de habilitacion de
        // cada sistema de particulas en la lista 'guns'
        foreach (GameObject gun in guns)
        {
            var part = gun.GetComponent<ParticleSystem>().emission;
            part.enabled = isActive;
        }
    }
}
